//loader ausblenden, wenn Seite vollständig geladen
window.addEventListener("load", function() { document.getElementById("loader").style.display = "none"; });

//Mouse-Tracking
window.addEventListener("mousemove", function () { drawCursor(event) });

async function getData() {
  //Fetchen des Datensatzes
  const response = await fetch('ProgrammingLanguages.csv');
  const data = await response.text();

  //Trennen der Zeilen, Abtrennen der 0. Zeile (Kopfzeile)
  const rows = data.split('\n').slice(1);


  var ProgrammingLanguagesArray = [];//Erstellen des Arrays für die Daten-Chunks
  for (var i=0; i < rows.length; i++) {
    rowColumns = rows[i].split(",");
    var WikiDataLink = rowColumns[0];
    var ProgrammingLanguageName = rowColumns[1];
    var CreationYear = rowColumns[2];
    ProgrammingLanguagesArray[i] = [WikiDataLink, ProgrammingLanguageName, CreationYear];

  }
  drawData(ProgrammingLanguagesArray); //Zeichenfunktion mit erstelltem Array aufrufen
}

async function drawData(DrawingDataArray) {

  //Gleichmäßigen Spaltenabstand berechnen
  const ScreenWidth = window.innerWidth;
  const NumberOfColumns = CountColumns(DrawingDataArray);
  const NumberOfSpacings = NumberOfColumns -1;
  const DotWidth = 15;
  const SpacingWidth = Math.floor((ScreenWidth - NumberOfColumns*DotWidth) / NumberOfSpacings);


  for (var i=0; i < DrawingDataArray.length; i++) {
    const DataSet = DrawingDataArray[i];  //Aktuelle Zeile
    const Name = DataSet[1];              //Name der Programmiersprache
    const Year = parseInt(DataSet[2]);    //Erscheinungsjahr

    const Dot = document.createElement("div");
    Dot.id = Name;
    Dot.classList.add("dot");

    if (i > 0) {                                          //Laden der Informationen des vorherigen Datensatzes
      var previousDataSet = DrawingDataArray[i-1];
      var previousName = previousDataSet[1].toString();
      var previousYear = parseInt(previousDataSet[2]);


      if (Year > previousYear) {
        //Wenn Jahreswechsel --> vertikal und horizontal als erstes Element, sonst über bestehendes Element der gleichen Jahreszahl schreiben (Abstand 15px)
        Dot.style.bottom = "70px";
        Dot.style.left = parseInt(document.getElementById(previousName).style.left) + DotWidth + SpacingWidth  + "px";

        //Hinzufügen neuer Jahresmarke bei Jahreswechsel
        const YearMark = document.createElement("div");
        YearMark.classList.add("YearMark");
        YearMark.id = Year;
        YearMark.style.left = parseInt(Dot.style.left)-15 + "px";
        YearMark.innerHTML = Year;
        document.body.appendChild(YearMark);
      } else {
        Dot.style.bottom = parseInt(document.getElementById(previousName).style.bottom) + 20 + "px";
        Dot.style.left = parseInt(document.getElementById(previousName).style.left) + "px";
      }
   //Erstes Element
   } else {
      Dot.style.bottom = "70px";
      Dot.style.left = "10px";



    //Hinzufügen ERSTER Jahresmarke
    const YearMark = document.createElement("div");
    YearMark.classList.add("YearMark");
    YearMark.id = Year;
    YearMark.style.left = parseInt(Dot.style.left)-15 + "px";
    YearMark.innerHTML = Year;
    document.body.appendChild(YearMark);
  }

  document.body.appendChild(Dot); //Aktuellen Dot in body hinzufügen


  //Klicken auf Dot öffnet Information der jeweiligen Programmiersprache
  Dot.addEventListener("click", function() { showProgrammingLanguageInformation(Dot.id); });

  //Dot mouseenter --> Name anzeigen
  Dot.addEventListener("mouseover", function () { showDotName(Dot.id); });

  //Dot mouseleave --> Name ausblenden
  Dot.addEventListener("mouseleave", function () { hideDotName(Dot.id); });

  //Delay in ms
  await new Promise(r => setTimeout(r,  0.01));
  }
}

function showProgrammingLanguageInformation(languageName) {

  //Dot Information erzeugen und hinzufügen
  const dotInformation = document.createElement("div");
  dotInformation.id = languageName + " - Information";
  dotInformation.classList.add("dotInformation");

  document.body.appendChild(dotInformation);


  //Name der Programmiersprache in DotInformation erzeugen und hinzufügen
  const dotInformationProgrammingLanguageChunk = document.createElement("center");
  dotInformationProgrammingLanguageChunk.innerHTML = languageName;
  dotInformation.appendChild(dotInformationProgrammingLanguageChunk);

  //Dot Information-Content-Fläche erzeugen
  const dotInformationContent = document.createElement("iframe");
  dotInformationContent.id = languageName + " - Content";
  dotInformationContent.classList.add("dotInformationContent");

  //I-Frame unter Name der Programmiersprache platzieren
  dotInformationContent.style.height = parseInt(100* ((0.9 * window.innerHeight - 90) / (0.9 * window.innerHeight)) ).toString() + "%";


  dotInformationContent.src = "https://en.m.wikipedia.org/wiki/" + languageName;

  //Wenn Frame aufgerufen --> loader anzeigen
  document.getElementById("loader").style.display = "block";

  //Frame geladen --> loader ausblenden
  dotInformationContent.addEventListener("load", function() { document.getElementById("loader").style.display = "none"; });

 dotInformation.appendChild(dotInformationContent);


  //ESC Taste schließt Informationsfenster
  window.addEventListener("keydown", event => { if (event.keyCode === 27) { hideProgrammingLanguageInformation(languageName); } });


  //Close-Button des DotInformation-Fensters erzeugen und hinzufügen
  const dotInformationCloseButton = document.createElement("div");
  dotInformationCloseButton.id = languageName + " - CloseButton";
  dotInformationCloseButton.classList.add("dotInformationCloseButton");
  dotInformationCloseButton.innerText = "Fertig";


  //Klicken auf CloseButton schließt Informationsfenster
  dotInformationCloseButton.addEventListener("click", function() {hideProgrammingLanguageInformation(languageName); });
  dotInformation.appendChild(dotInformationCloseButton);


  //Informationsfenster anzeigen
  dotInformation.style.display = "block";
  dotInformation.style.animation = "dotInformationStartAnimation 1s";

 }

//Asynchron, da Close-Animation vollendet werden muss, ehe display auf none gesetzt wird --> timer = asynchrone Funktion
async function hideProgrammingLanguageInformation(languageName) {
  const languageNameInformation = languageName + " - Information";
  document.getElementById(languageNameInformation).style.animation = "dotInformationCloseAnimation 0.2s";
  //Warten, bis Animation beendet
  await new Promise(r => setTimeout(r,   150));
  document.getElementById(languageNameInformation).style.display = "none";

  //Informationsfenster aus DOM entfernen
  document.body.removeChild(document.getElementById(languageNameInformation));
}


//Anzahl der vorhandenen Spalten berechnen --> == Anzahl unterschiedlicher Jahreszahlen
function CountColumns(DataArray) {
  var ColumnCounter = 0;
  for (i = 1; i < DataArray.length; i++) {
    OldDataLine = DataArray[i-1];
    NewDataLine = DataArray[i];
    if (NewDataLine[2] > OldDataLine[2]) {
      ColumnCounter +=1;
    }
  }
  return ColumnCounter;
}

function drawCursor(e) {
    //Offset 5px, damit Dots anklickbar bleiben
    document.getElementById("cursorLine").style.left = e.clientX - 5 + "px";
    document.getElementById("cursorBall").style.left = e.clientX - 5 + "px";
    document.getElementById("cursorBall").style.top = e.clientY - 5 + "px";
}

//Name der fokussierten Programmierspache als Fähnchen anzeigen
async function showDotName(dotName) {

    const dotNameLabelText = document.createElement("center");
    dotNameLabelText.innerText = dotName;
    dotNameLabelText.style.backgroundColor = "#FFFFFF";

    document.getElementById("dotNameLabel").appendChild(dotNameLabelText);
    document.getElementById("dotNameLabel").style.left = parseInt(document.getElementById(dotName).style.left) + 25 + "px";
    document.getElementById("dotNameLabel").style.bottom = parseInt(document.getElementById(dotName).style.bottom)  -12.5 + "px";
    document.getElementById("dotNameLabel").style.animation = "dotNameLabelRollOut 0.3s";
    document.getElementById("dotNameLabel").style.display = "block";

}

//Name der Programmiersprache als Fähnchen ausblenden, wenn nicht mehr fokussiert
async function hideDotName(dotName) {

    document.getElementById("dotNameLabel").innerHTML = null;
    document.getElementById("dotNameLabel").style.display = "none";

}

getData();




